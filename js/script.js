
//1) First approach - forEach
//apply 'click' handler upon body of the document
//sort out all buttons and check if keycode of the event coincides with each of btn inner text.
//appeal to the active class with changed background color, and if it is not active - remove 'active' class.

// document.body.addEventListener('keyup', (event) => {
//     document.querySelectorAll('.btn').forEach((btn) => {
//         const contentKey = btn.textContent;
//         if (event.key.toUpperCase() === contentKey.toUpperCase()) {
//             const activeBtn = document.querySelector('.active');
//             if (activeBtn !== null) {
//                 activeBtn.classList.remove('active');
//             }
//             btn.classList.add('active');
//         }
//     });
// });



//1) Second approach - arr.prototype.find()
const buttons = document.querySelectorAll('.btn');
const btnArr = Array.from(buttons);

document.body.addEventListener('keyup', (event) => {
   let checkKey = btnArr.find(el => el.innerText.toUpperCase() === event.key.toUpperCase());
    // console.log(checkKey);
    if (checkKey) {
        const activeBtn = document.querySelector('.active');
        if (activeBtn !== null) {
            activeBtn.classList.remove('active');
        }
        checkKey.classList.add('active');
    }else{
        console.log('There is no such key')
    }
});

